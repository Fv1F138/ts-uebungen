// A1 - Gebe alle Zahlen von 1 - 10 mit einer Schleife deiner Wahl aus

// A2 - Gebe alle Zahlen aus dem Array a2_zahlen mit einer for, for each und while Schleife aus
const a2_zahlen: number[] = [1,3,5,7,9,12]

// A3 - Fülle das Array a3_rand_zahlen mit 20 zufälligen ganzen Zahlen und sortiere es aufsteigend mit einem bubble sort algorythmus
// https://en.wikipedia.org/wiki/Bubble_sort#/media/File:Bubble-sort-example-300px.gif
const a3_rand_zahlen: number[] = [];

/*
A4 - Erstelle eine Funktion, die das Pascalsche Dreieck bis zur Stufe n berechnet
Bsp:

> pascal(4);

            1    
          1   1    
        1   2   1    
      1   3   3   1    
    1   4   6   4   1
*/

console.log("executed");